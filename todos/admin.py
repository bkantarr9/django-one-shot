from django.contrib import admin
from todos.models import TodoList, TodoItem

# ToDoList model with admin so that I can see it in Django admin site
# display the name and id as colomns in the model's list display
@admin.register(TodoList)
class TodoListAdmin(admin.ModelAdmin):
    TodoList = [
        "title",
        "id",
    ]

@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    TodoList = [
        "task",
        "due_date",
        "is_completed",
        "id",
    ]
    


